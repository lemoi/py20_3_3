import requests
from pprint import pprint
import json

PATH_ENV = 'env.example.json'

class User:
  def __init__(self, token, id=None):
    self.token = token

    if id == None:
      self.info = self.get_user_info()
      self.id = self.info['id']
    else:
      self.id = id

  def appeal_api(self, url, params):
    union_params = {
      'access_token': self.token,
      'v': '5.92',
    }
    union_params.update(params)
    response = requests.get(url, params=union_params)

    result = response.json()
    if 'error'in result:
      print('Ошибка обращения к API VK')
      pprint(result['error'])
      raise

    return result.get('response')

  def __str__(self):
    return f'https://vk.com/id{self.id}'

  def __and__(self, other):
    params = {
      'source_uid': self.id,
      'target_uid': other.id
    }

    result = self.appeal_api('https://api.vk.com/method/friends.getMutual', params)
    users = [User(self.token, user_id) for user_id in result]

    return users;

  def __getattr__(self, name):
    if name == 'info':
      self.info = self.get_user_info()
      return self.info

  def get_user_info(self, name_case=None):
    params = {
      'fields': 'domain'
    }

    if self.id != None:
      params['user_ids'] = self.id

    if name_case != None:
      params['name_case'] = name_case

    result = self.appeal_api('https://api.vk.com/method/users.get', params)
    return result[0]

  def get_status(self):
    params = {
      'user_id': self.id
    }

    result = self.appeal_api('https://api.vk.com/method/status.get', params)
    return result.get('text')

def get_env():
  with open(PATH_ENV, 'r', encoding='utf-8') as file:
    env = json.load(file)

  return env

def update_env(env):
  with open(PATH_ENV, 'w', encoding='utf-8') as file:
    json.dump(env, file, indent=2)


if __name__ == '__main__':
  env = get_env()
  # Действительный токен необходимо поместить в параметр token, либо установить тут
  # env['token'] = ''

  # user = User(env['token'])
  # print(user)
  # pprint(user.info)

  user1 = User(env['token'], env['user_id_1'])
  user2 = User(env['token'], env['user_id_2'])
  print(user1.get_status())
  print(user2.get_status())

  info1 = user1.get_user_info(name_case='gen')
  info2 = user2.get_user_info(name_case='gen')
  print(f"Общие друзья {info1['first_name']} {info1['last_name']} (id{info1['id']}) и {info2['first_name']} {info2['last_name']} (id{info2['id']}): ")
  mutual_friends = user1 & user2
  for user in mutual_friends:
    print(user)
    # pprint(user.info)  # закоментировал получение информации по пользователю, чтобы не мучить сервер VK запросами
